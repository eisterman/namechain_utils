use criterion::{black_box, criterion_group, criterion_main, Criterion, BenchmarkId};
use namechain_utils::block::Block;
use namechain_utils::network::NetMessage;
use sha3::{Sha3_256, Digest};
use crate::bench_utils::{get_default_block, TestConfig};

mod bench_utils;

pub fn check_valid_block_syntax_bench(c: &mut Criterion) {
    let config = bench_utils::TestConfig::default();
    let block_str = bench_utils::DEFAULT_BLOCK_STR;
    c.bench_with_input(BenchmarkId::new("check_valid_block", "default"), &(config, block_str), |b, i| b.iter(|| Block::check_valid_block_syntax(&i.0, i.1)));
}

fn only_str_hashing_bench() -> String {
    let mut hasher = Sha3_256::new();
    hasher.input(black_box(bench_utils::DEFAULT_BLOCK_STR));
    format!("{:064x}", hasher.result())
}

pub fn block_str_hashing_check_overhead_bench(c: &mut Criterion) {
    let mut group = c.benchmark_group("block_str_hashing_check_overhead");
    let block = get_default_block();
    group.bench_function("only_hashing", |b| b.iter_with_large_drop(only_str_hashing_bench));
    group.bench_with_input(BenchmarkId::from_parameter("block_overheaded_hashing"), &block,|b, block| b.iter_with_large_drop(|| block.str_hash()));
    group.finish();
}

pub fn check_difficulty_block_bench(c: &mut Criterion) {
    let config = TestConfig::default();
    let block = get_default_block();
    c.bench_with_input(BenchmarkId::new("check_difficulty_block", "default"), &(config, block), |b, (cfg,blk)| b.iter(|| blk.check_difficulty_hash(cfg)));
}

//TODO: Ottimizza la creazione del blocco

criterion_group!(block_benches, check_valid_block_syntax_bench, block_str_hashing_check_overhead_bench, check_difficulty_block_bench);

pub fn netmsg_parse_blocks(c: &mut Criterion) {
    let mut group = c.benchmark_group("netmsg_parse_block");
    let mut blocks: Vec<(String, String)> = Vec::new();
    blocks.push(("000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c".into(), "wrong_format_1".into()));
    blocks.push(("000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase".into(), "wrong_format_2".into()));
    blocks.push(("000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase ".into(), "wrong_format_2_with_whitespace".into()));
    blocks.push(("000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase 261805".into(), "right_block".into()));
    blocks.push(("000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase 261805 aye".into(), "wrong_format_4".into()));
    blocks.push(("000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbaseXXXXXXXXXXXXXXXXXXXXX 261805".into(), "wrong_length".into()));
    let cfg = TestConfig::default();
    for (blk, name) in blocks.iter() {
        let cfg_c = cfg.clone();
        group.bench_with_input(BenchmarkId::from_parameter(name), &(cfg_c, blk), |b, (cfg, blk)| {
            b.iter_with_large_drop(|| NetMessage::from_str(cfg, blk));
        });
    }
    group.finish();
}

criterion_group!(netmsg_benches, netmsg_parse_blocks);

criterion_main!(block_benches, netmsg_benches);