use std::fmt::Formatter;
use itertools::Itertools;
use sha3::{Digest,Sha3_256};
use primitive_types::U256;

use crate::errors::BlockError;
use crate::config::CommonConfig;

#[derive(Debug, Eq, PartialEq, Clone, Default)]
pub struct Block {
    prevhash: String,
    name: String,
    nounce: u64
}

impl Block {
    pub fn check_valid_block_syntax(config: &dyn CommonConfig, block: &str) -> bool {
        block.trim().split_whitespace().count() == 3 &&
            block.len() <= config.get_block_max_len() &&
            block.trim().split_whitespace().next().unwrap().len() == 64
    }

    pub fn from_str(config: &dyn CommonConfig, block: &str) -> Result<Block,BlockError> {
        if Block::check_valid_block_syntax(config, block) {
            let (prevhash, name, nounce) = block.trim().split_whitespace().next_tuple().unwrap();
            let nounce: u64 = nounce.parse().map_err(|_| BlockError::new(block.trim()))?;
            return Ok(Block{prevhash: prevhash.to_string(), name: name.to_string(), nounce});
        }
        Err(BlockError::new(block))
    }

    pub fn new(config: &dyn CommonConfig, prevhash: &str, name: &str, nounce: u64) -> Result<Block,BlockError> {
        let composition = format!("{} {} {}", prevhash, name, nounce);
        Block::from_str(config,composition.as_str())
    }

    #[inline]
    pub fn get_prevhash(&self) -> String {
        self.prevhash.clone()
    }

    #[inline]
    pub fn get_name(&self) -> String {
        self.name.clone()
    }

    #[inline]
    pub fn get_nounce(&self) -> u64 {
        self.nounce
    }

    pub fn str_hash(&self) -> String {
        let mut hasher = Sha3_256::new();
        hasher.input(self.to_string());
        format!("{:064x}", hasher.result())
    }

    #[inline]
    pub fn decimal_hash(&self) -> U256 {
        let str = self.str_hash();
        hex::decode(str).unwrap().as_slice().into()
    }

    #[inline]
    pub fn next_nounce(&mut self) -> u64 {
        self.nounce += 1;
        self.nounce
    }

    pub fn check_difficulty_hash(&self, config: &dyn CommonConfig) -> bool {
        let block_hash = self.decimal_hash();
        let difficulty = config.get_difficulty();
        block_hash < difficulty
    }
}

impl std::fmt::Display for Block {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {} {}", self.prevhash, self.name, self.nounce)
    }
}

#[cfg(test)]
mod tests {
    use crate::tests_utils::*;
    use super::{Block,U256};

    #[test]
    fn build_good_block() {
        let block = get_default_block();
        assert_eq!(block, Block{prevhash: "000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c".to_string(), name: "coinbase".to_string(), nounce: 261805});
    }

    #[test]
    fn build_bad_empty_block() {
        assert!(Block::from_str(&TestConfig::default(), "").is_err());
    }

    #[test]
    fn build_bad_format_block() {
        let config = TestConfig::default();
        assert!(Block::from_str(&config, "000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c").is_err());
        assert!(Block::from_str(&config, "000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase").is_err());
        assert!(Block::from_str(&config, "000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase ").is_err());
        assert!(Block::from_str(&config, "000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase 261805 aye").is_err());
    }

    #[test]
    fn build_bad_length_block() {
        assert!(Block::from_str(&TestConfig::default(), "000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbaseXXXXXXXXXXXXXXXXXXXXX 261805").is_err());
    }

    #[test]
    fn test_hexhash() {
        let b = get_default_block();
        assert_eq!(b.str_hash(), "00005ac594667d5587bb7d2baba05cd3f3cdc566150b575e2f3b094d9182ea59".to_string());
    }

    #[test]
    fn test_dechash() {
        let b = get_default_block();
        assert_eq!(b.decimal_hash(), U256::from_dec_str("626483909085516712391186229673189504941533734455093053853053650202716761").unwrap());
    }

    #[test]
    fn check_difficulty_good_block() {
        let b = get_default_block();
        assert!(b.check_difficulty_hash(&TestConfig::default()));
    }

    #[test]
    fn check_difficulty_bad_block() {
        let b = Block::from_str(&TestConfig::default(), "010029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase 261805").unwrap();
        assert!(!b.check_difficulty_hash(&TestConfig::default()));
    }
}