use std::error::Error;
use std::fmt::{Display,Formatter};

#[derive(Debug)]
pub struct NetworkError {
    host: String,
    msg: String
}

impl NetworkError {
    pub fn new(host: &str, msg: &str) -> Self {
        NetworkError{host: host.to_string(), msg: msg.to_string()}
    }
}

impl Display for NetworkError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Generic Network Error - host: {} - message:\n{}", self.host, self.msg)
    }
}

impl Error for NetworkError {}


#[derive(Debug)]
pub struct BlockError {
    block_str: String
}

impl BlockError {
    pub fn new(block_str: &str) -> Self {
        BlockError{block_str: block_str.to_string()}
    }
}

impl Display for BlockError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Error in the formattation of a block - block_str = \"{}\"", self.block_str)
    }
}

impl Error for BlockError {}


#[derive(Debug)]
pub struct NetParseError {
    data: Vec<u8>
}

impl NetParseError {
    pub fn new(data: &[u8]) -> Self {
        NetParseError{data: data.to_vec()}
    }

    pub fn get_data(&self) -> Vec<u8> {
        self.data.clone()
    }
}

impl Display for NetParseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Error when parsing some data in byte format: {:?}", self.data)
    }
}

impl Error for NetParseError {}


#[derive(Debug)]
pub struct UTF8FormatError {
    data: Vec<u8>
}

impl UTF8FormatError {
    pub fn new(data: &[u8]) -> Self {
        UTF8FormatError {data: data.to_vec()}
    }

    pub fn get_data(&self) -> Vec<u8> {
        self.data.clone()
    }
}

impl Display for UTF8FormatError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Error while formatting some data from bytes to a UTF8 string: {:?}", self.data)
    }
}

impl Error for UTF8FormatError {}
//TODO: Create various type of BlockError (Difficulty, Generic)