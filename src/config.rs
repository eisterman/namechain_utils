use primitive_types::U256;

// Trait with method common to all the config struct
pub trait CommonConfig {
    fn get_difficulty(&self) -> U256;
    fn get_block_max_len(&self) -> usize;
}
