use std::error::Error;
use std::fmt::Formatter;
use std::io::{Read,Write};
use std::net::TcpStream;
use primitive_types::U256;

use crate::block::Block;
use crate::config::CommonConfig;
use crate::errors::{NetParseError, UTF8FormatError};

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum NetMessage {
    AskLastBlock,
    ValidBlockAppended,
    Block(Block),
    WrongPrevHash,
    WrongDifficulty(U256),
    InvalidBlockOrOpcode,
    EmptyMessage
}

impl NetMessage {
    pub fn get_message(&self) -> Vec<u8> {
        use NetMessage::*;
        match self {
            AskLastBlock => {
                b"ASK_LAST_BLOCK".to_vec()
            }
            ValidBlockAppended => {
                b"VALID_BLOCK_APPENDED".to_vec()
            }
            Block(b) => {
                b.to_string().as_bytes().to_vec()
            }
            WrongPrevHash => {
                b"WRONG_PREV_HASH".to_vec()
            }
            WrongDifficulty(server_difficulty) => {
                let string = format!("WRONG_DIFFICULTY {}", server_difficulty);
                string.as_bytes().to_vec()
            }
            InvalidBlockOrOpcode => {
                b"INVALID_BLOCK_OR_OPCODE".to_vec()
            }
            EmptyMessage => {
                b"".to_vec()
            }
        }
    }

    pub fn from_message(config: &dyn CommonConfig, msg: &[u8]) -> Result<Self,NetParseError> {
        use NetMessage::*;
        match msg {
            b"" => {
                Ok(EmptyMessage)
            }
            b"ASK_LAST_BLOCK" => {
                Ok(AskLastBlock)
            }
            b"VALID_BLOCK_APPENDED" => {
                Ok(ValidBlockAppended)
            }
            b"WRONG_PREV_HASH" => {
                Ok(WrongPrevHash)
            }
            b"INVALID_BLOCK_OR_OPCODE" => {
                Ok(InvalidBlockOrOpcode)
            }
            x if crate::block::Block::check_valid_block_syntax(config, std::str::from_utf8(x).unwrap()) => {
                Ok(Block(crate::block::Block::from_str(config, std::str::from_utf8(x).unwrap()).or_else(|_| Err(NetParseError::new(x)) )?))
            }
            x => {
                let s = std::str::from_utf8(x).unwrap();
                if s.split_whitespace().count() == 2 {
                    let mut elem_iter = s.split_whitespace();
                    if elem_iter.next().unwrap() == "WRONG_DIFFICULTY" {
                        let right_difficulty = U256::from_dec_str(elem_iter.next().unwrap());
                        if let Ok(rd) = right_difficulty {
                            return Ok(WrongDifficulty(rd));
                        }
                    }
                }
                Err(NetParseError::new(x))
            }
        }
    }

    #[inline]
    pub fn from_str(config: &dyn CommonConfig, msg: &str) -> Result<Self,NetParseError> {
        Self::from_message(config, msg.as_bytes())
    }

    pub fn send_to(&self, stream: &mut TcpStream) -> std::io::Result<usize> {
        let msg = self.get_message();
        stream.write(msg.as_ref())
    }

    pub fn receive_from(config: &dyn CommonConfig, stream: &mut TcpStream) -> Result<Self,Box<dyn Error>> {
        let mut data = vec![0_u8; config.get_block_max_len()]; // using 100 byte buffer
        match stream.read(&mut data) {
            Ok(0_usize) => {
                Ok(NetMessage::EmptyMessage)
            }
            Ok(size) => {
                let str_data = String::from_utf8(data[..size].to_vec()).map_err(|_| {
                    Box::new(UTF8FormatError::new(&data[..size]))
                })?;
                match Self::from_str(config, str_data.as_str()) {
                    Ok(m) => {
                        Ok(m)
                    }
                    Err(e) => {
                        Err(Box::new(e))
                    }
                }
            },
            Err(e) => {
                Err(Box::new(e))
            }
        }
    }
}

impl std::fmt::Display for NetMessage {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let msg = self.get_message();
        let msg = std::str::from_utf8(msg.as_slice()).unwrap();
        write!(f, "{}", msg)
    }
}

#[cfg(test)]
mod tests {
    use crate::tests_utils::*;
    use super::{U256,NetMessage};
    use std::net::{TcpStream, TcpListener};
    use std::io::Write;

    #[test]
    fn from_str_valid_block() {
        let c = TestConfig::default();
        let b = get_default_block();
        assert_eq!(NetMessage::from_str(&c, b.to_string().as_str()).unwrap(), NetMessage::Block(b));
    }

    #[test]
    fn from_str_unvalid_block() {
        let c = TestConfig::default();
        let mut blocks: Vec<String> = Vec::new();
        blocks.push("000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c".into());
        blocks.push("000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase".into());
        blocks.push("000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase ".into());
        blocks.push("000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbase 261805 aye".into());
        blocks.push("000029b37bb05989358847e04c4ea8ce40c6f666c0b6a68b93581785882b136c coinbaseXXXXXXXXXXXXXXXXXXXXX 261805".into());
        for str_blk in blocks {
            assert!(NetMessage::from_str(&c, str_blk.as_str()).is_err(), format!("The string {} is valid!", str_blk));
        }
    }

    #[test]
    fn from_str_bad_message() {
        let c = TestConfig::default();
        assert!(NetMessage::from_str(&c, "AyEyE_bRaAzOrF").is_err());
        assert!(NetMessage::from_str(&c, "AyEyE bRaAzOrF").is_err());
    }

    #[test]
    fn from_str_wrong_difficulty_msg() {
        let c = TestConfig::default();
        assert_eq!(NetMessage::from_str(&c, "WRONG_DIFFICULTY 21").unwrap(), NetMessage::WrongDifficulty(21.into()));
        assert_eq!(NetMessage::from_str(&c, "WRONG_DIFFICULTY 883423532389192164791648750371459257913741948437809479060803100646309888").unwrap(),
                   NetMessage::WrongDifficulty(U256::from_dec_str("883423532389192164791648750371459257913741948437809479060803100646309888").unwrap()));
    }

    #[test]
    fn from_str_wrong_difficulty_invalid() {
        let c = TestConfig::default();
        assert!(NetMessage::from_str(&c, "WRONG_DIFFICULTY -3").is_err());
        assert!(NetMessage::from_str(&c, "WRONG_DIFFICULTY ayeye_braazorf").is_err());
    }

    #[test]
    fn from_str_empty() {
        let c = TestConfig::default();
        let expected = NetMessage::EmptyMessage;
        assert_eq!(NetMessage::from_str(&c, "").unwrap(), expected);
    }

    #[test]
    fn from_str_ask_last_block() {
        let c = TestConfig::default();
        let expected = NetMessage::AskLastBlock;
        assert_eq!(NetMessage::from_str(&c, "ASK_LAST_BLOCK").unwrap(), expected);
    }

    #[test]
    fn from_str_valid_block_appended() {
        let c = TestConfig::default();
        let expected = NetMessage::ValidBlockAppended;
        assert_eq!(NetMessage::from_str(&c, "VALID_BLOCK_APPENDED").unwrap(), expected);
    }

    #[test]
    fn from_str_wrong_prev_hash() {
        let c = TestConfig::default();
        let expected = NetMessage::WrongPrevHash;
        assert_eq!(NetMessage::from_str(&c, "WRONG_PREV_HASH").unwrap(), expected);
    }

    #[test]
    fn from_str_invalid_block_or_opcode() {
        let c = TestConfig::default();
        let expected = NetMessage::InvalidBlockOrOpcode;
        assert_eq!(NetMessage::from_str(&c, "INVALID_BLOCK_OR_OPCODE").unwrap(), expected);
    }

    async fn listen_str(port: u16) -> std::io::Result<String> {
        use crate::config::CommonConfig;
        use std::io::Read;
        assert_ne!(port,0);
        let listen_addr = format!("0.0.0.0:{}", port);
        let listener = TcpListener::bind(listen_addr).unwrap();
        let c = TestConfig::default();
        let stream_tuple = listener.accept();
        match stream_tuple {
            Ok((mut stream, _addr)) => {
                let mut data = vec![0_u8; c.get_block_max_len()];
                match stream.read(&mut data) {
                    Ok(size) => {
                        let str_data = String::from_utf8(data[..size].to_vec()).unwrap();
                        Ok(str_data)
                    }
                    Err(e) => {
                        Err(e)
                        /* connection failed */
                    }
                }
            }
            Err(e) => {
                Err(e)
                /* connection failed */
            }
        }
    }

    async fn send_netmessage(msg: NetMessage, port: u16, expected_msg: &str) {
        let server_str = listen_str(port).await;
        // Wait for the start of the server listener
        std::thread::sleep(std::time::Duration::from_millis(10));
        let mut send_stream = TcpStream::connect(format!("0.0.0.0:{}", port)).unwrap();
        msg.send_to(&mut send_stream).unwrap();
        assert_eq!(server_str.unwrap().as_str(), expected_msg);
    }

    #[test]
    fn send_emptymsg() {
        let netmsg = NetMessage::EmptyMessage;
        let expected = "";
        let executor = futures::executor::ThreadPoolBuilder::default().pool_size(2).create().unwrap();
        executor.spawn_ok(send_netmessage(netmsg, 4301, expected));
    }

    #[test]
    fn send_asklastblock() {
        let netmsg = NetMessage::AskLastBlock;
        let expected = "ASK_LAST_BLOCK";
        let executor = futures::executor::ThreadPoolBuilder::default().pool_size(2).create().unwrap();
        executor.spawn_ok(send_netmessage(netmsg, 4302, expected));
    }

    #[test]
    fn send_validblockappended() {
        let netmsg = NetMessage::ValidBlockAppended;
        let expected = "VALID_BLOCK_APPENDED";
        let executor = futures::executor::ThreadPoolBuilder::default().pool_size(2).create().unwrap();
        executor.spawn_ok(send_netmessage(netmsg, 4303, expected));
    }

    #[test]
    fn send_wrongprevhash() {
        let netmsg = NetMessage::WrongPrevHash;
        let expected = "WRONG_PREV_HASH";
        let executor = futures::executor::ThreadPoolBuilder::default().pool_size(2).create().unwrap();
        executor.spawn_ok(send_netmessage(netmsg, 4304, expected));
    }

    #[test]
    fn send_invalidblockoropcode() {
        let netmsg = NetMessage::InvalidBlockOrOpcode;
        let expected = "INVALID_BLOCK_OR_OPCODE";
        let executor = futures::executor::ThreadPoolBuilder::default().pool_size(2).create().unwrap();
        executor.spawn_ok(send_netmessage(netmsg, 4305, expected));
    }

    #[test]
    fn send_block() {
        let netmsg = NetMessage::Block(get_default_block());
        let expected = DEFAULT_BLOCK_STR;
        let executor = futures::executor::ThreadPoolBuilder::default().pool_size(2).create().unwrap();
        executor.spawn_ok(send_netmessage(netmsg, 4306, expected));
    }

    #[test]
    fn send_wrongdifficulty() {
        let netmsg = NetMessage::WrongDifficulty(634.into());
        let expected = "WRONG_DIFFICULTY 634";
        let executor = futures::executor::ThreadPoolBuilder::default().pool_size(2).create().unwrap();
        executor.spawn_ok(send_netmessage(netmsg, 4307, expected));
    }

    async fn send_str(msg: &str, port: u16) {
        // Wait for the start of the server listener
        std::thread::sleep(std::time::Duration::from_millis(10));
        let mut stream = TcpStream::connect(format!("0.0.0.0:{}", port)).unwrap();
        let _written = stream.write(msg.as_bytes()).unwrap();
    }

    async fn receive_netmessage(msg_str: &str, port: u16, expected: NetMessage) {
        let c = TestConfig::default();
        let listener = TcpListener::bind(format!("0.0.0.0:{}", port)).unwrap();
        send_str(msg_str, port).await;
        let (mut stream, _) = listener.accept().unwrap();
        let result = NetMessage::receive_from(&c, &mut stream).unwrap();
        assert_eq!(result, expected);
    }

    #[test]
    fn receive_emptymsg() {
        let netexpected = NetMessage::EmptyMessage;
        let msg = "";
        let executor = futures::executor::ThreadPoolBuilder::default().pool_size(2).create().unwrap();
        executor.spawn_ok(receive_netmessage(msg, 4401, netexpected));
    }

    #[test]
    fn receive_asklastblock() {
        let netexpected = NetMessage::AskLastBlock;
        let msg = "ASK_LAST_BLOCK";
        let executor = futures::executor::ThreadPoolBuilder::default().pool_size(2).create().unwrap();
        executor.spawn_ok(receive_netmessage(msg, 4402, netexpected));
    }

    #[test]
    fn receive_validblockappended() {
        let netexpected = NetMessage::ValidBlockAppended;
        let msg = "VALID_BLOCK_APPENDED";
        let executor = futures::executor::ThreadPoolBuilder::default().pool_size(2).create().unwrap();
        executor.spawn_ok(receive_netmessage(msg, 4403, netexpected));
    }

    #[test]
    fn receive_wrongprevhash() {
        let netexpected = NetMessage::WrongPrevHash;
        let msg = "WRONG_PREV_HASH";
        let executor = futures::executor::ThreadPoolBuilder::default().pool_size(2).create().unwrap();
        executor.spawn_ok(receive_netmessage(msg, 4404, netexpected));
    }

    #[test]
    fn receive_invalidblockoropcode() {
        let netexpected = NetMessage::InvalidBlockOrOpcode;
        let msg = "INVALID_BLOCK_OR_OPCODE";
        let executor = futures::executor::ThreadPoolBuilder::default().pool_size(2).create().unwrap();
        executor.spawn_ok(receive_netmessage(msg, 4405, netexpected));
    }

    #[test]
    fn receive_block() {
        let netexpected = NetMessage::Block(get_default_block());
        let msg = DEFAULT_BLOCK_STR;
        let executor = futures::executor::ThreadPoolBuilder::default().pool_size(2).create().unwrap();
        executor.spawn_ok(receive_netmessage(msg, 4406, netexpected));
    }

    #[test]
    fn receive_wrongdifficulty() {
        let netexpected = NetMessage::WrongDifficulty(634.into());
        let msg = "WRONG_DIFFICULTY 634";
        let executor = futures::executor::ThreadPoolBuilder::default().pool_size(2).create().unwrap();
        executor.spawn_ok(receive_netmessage(msg, 4407, netexpected));
    }
}
